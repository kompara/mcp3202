# MCP3202 library
This is a simple library that can work with MCP3202.

## Installation
### Arduino
#### 'Static' install
Install the same as any other Arduino library.

1. Copy `arduino/libraries/MCP3202` into `/{path-to-Arduino-folder}/Sketches/libraries`

In this case you will always need to rewrite files when you version is released. To make your life a bit easier checkout 'dynamic' installation.

#### 'Dynamic' installation
1. GIT-clone MCP3202 anywhere on your drive
2. Run this...
```
$ cd /{path-to-Arduino-folder}/Sketches/libraries
$ ln -s /{path-to-MCP3202-folder}/arduino/libraries/MCP3202 MCP3202
```

## Connecting
This is how to connect your Arduino to the MCP3202

|MCP3202          |     Mega     |   Uno      |
|:------------    |:------------:|:----------:|
|(1) CS/SHDN      |53            |   10       |
|(2)  CH0         |signal 0      |   signal 0 |
|(3)  CH1         |signal 1      |   signal 1 |
|(4)  Vss         |GND           |   GND      |
|(5)  Vdd/Vref    |3.3V/5V       |   3.3V/5V  |
|(6)  CLK         |52            |   13       |
|(7)  Dout        |50            |   12       |
|(8)  Din         |51            |   11       |

## Usage
Run Arduino IDE or similar and open examples to see it working:

* Simple.ino - read 12-bit data from MCP3202

* Complex.ino - example how to change MCP3202 resolution

## Licence
### Released under GNU General Public License v2.0
You may copy, distribute and modify the software as long as you track changes/dates in source files. Any modifications to or software including (via compiler) GPL-licensed code must also be made available under the GPL along with build & install instructions.