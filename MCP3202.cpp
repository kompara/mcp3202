/*
 * MCP3202
 *  This is the main file of MCP3202 library.
 *
 * @file MCP3202.ino
 * @author Tomaz Kompara <tomaz@kompara.si>
 * @date 23.6.2016
 * @copyright (c) 2016 Tomaz Kompara
 * 
 * Released under GNU General Public License v2.0
 * 
 * You may copy, distribute and modify the software as long as you track 
 * changes/dates in source files. Any modifications to or software including 
 * (via compiler) GPL-licensed code must also be made available under the GPL 
 * along with build & install instructions.
 */

#include <SPI.h>
#include <MCP3202.h>

MCP3202::MCP3202(const byte CS){
  _CS = CS;
  pinMode(_CS, OUTPUT);
  // If the device was powered up with the CS pin low, it must be brought high and
  // back low to initiate communication.
  digitalWrite(_CS, LOW);
  digitalWrite(_CS, HIGH);
  resolution = 10; // Set default resolution - 10bit
}

void MCP3202::begin(){
  SPI.begin();
  SPI.setDataMode(SPI_MODE0); // Set SPI mode 0,0
  SPI.setBitOrder(MSBFIRST); // Send MSB first
  SPI.setClockDivider(SPI_CLOCK_DIV4); // Set = 16MHz / 4 = 4000kHz
}

void MCP3202::begin(const byte adc_resolution){
  resolution = adc_resolution;
  begin();
}

int MCP3202::read(const byte channel){
  if(resolution == 8)       return read_8bit(channel);
  else if(resolution > 8 && resolution < 12) return read_9_11bit(channel);
  else if(resolution == 12) return read_12bit(channel);

  Serial.println("Error: set resolution between 8 and 12 bit.");
  return -1;
}

int MCP3202::read(const byte channel, const byte adc_resolution){
  resolution = adc_resolution;
  return read(channel);
}

int MCP3202::read_8bit(const byte channel){
  int value = 0;
  digitalWrite(_CS, LOW);
  SPI.transfer(0b00011010 | ((channel & 0x01) << 2));
  value = SPI.transfer(0x00);
  digitalWrite(_CS, HIGH);
  return (value & 0x00ff);
}

int MCP3202::read_9_11bit(const byte channel){
  int value = 0;
  byte a = resolution - 9;
  digitalWrite(_CS, LOW);
  value = SPI.transfer((0b00110100 << a) | ((channel & 0x01) << (3+a)));
  value = ((byte)value) << 8 | SPI.transfer(0x00);
  digitalWrite(_CS, HIGH);
  return (value & (int)pow(2,resolution)-1);
}

int MCP3202::read_12bit(const byte channel){
  int value = 0;
  digitalWrite(_CS, LOW);
  SPI.transfer(0x01); // Start bit
  value = SPI.transfer((0b10100000) | ((channel & 0x01) << 5));
  value = ((byte)value << 8) | SPI.transfer(0x00);
  digitalWrite(_CS, HIGH);
  return (value & 0x0fff);
}