/*
 * MCP3202
 *  This is a simple example of using MCP3202 library.
 *
 * @file MCP3202.ino
 * @author Tomaz Kompara <tomaz@kompara.si>
 * @date 23.6.2016
 * @copyright (c) 2016 Tomaz Kompara
 * 
 * Released under GNU General Public License v2.0
 * 
 * You may copy, distribute and modify the software as long as you track 
 * changes/dates in source files. Any modifications to or software including 
 * (via compiler) GPL-licensed code must also be made available under the GPL 
 * along with build & install instructions.
 */

#include <SPI.h>
#include <MCP3202.h>

MCP3202 adc(53); // Pin 53 is for Arduino Mega 2560

void setup () {
  adc.begin();   // Begin the ADC converter with 12-bit resolution
  Serial.begin (115200);
}

void loop () {
  int value = adc.read(0); // Read ADC on channel 0
  Serial.print("ADC0: ");
  Serial.println(value);

  delay(200);
}