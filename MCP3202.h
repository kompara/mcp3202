/*
 * MCP3202
 *  This is the header file of MCP3202 library.
 *
 * @file MCP3202.h
 * @author Tomaz Kompara <tomaz@kompara.si>
 * @date 23.6.2016
 * @copyright (c) 2016 Tomaz Kompara
 * 
 * Released under GNU General Public License v2.0
 * 
 * You may copy, distribute and modify the software as long as you track 
 * changes/dates in source files. Any modifications to or software including 
 * (via compiler) GPL-licensed code must also be made available under the GPL 
 * along with build & install instructions.
 */

#ifndef MCP3202_h
#define MCP3202_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

class MCP3202{
  public:
    MCP3202(const byte CS);
    void begin();
    void begin(const byte adc_resolution);
    int read(const byte channel);
    int read(const byte channel, const byte adc_resolution);
    byte resolution; // set ADC resolution

  private:
    byte _CS; // chip select pin
    int read_8bit(const byte channel);
    int read_9_11bit(const byte channel);
    int read_12bit(const byte channel);
};

#endif